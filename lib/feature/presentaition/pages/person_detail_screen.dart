import 'package:flutter/material.dart';
import 'package:rick_and_morty/common/app_colors.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty/feature/presentaition/widgets/persont_cache_image_widget.dart';

class PersonDatailPage extends StatelessWidget {
  final PersonEntity person;

  const PersonDatailPage({Key? key, required this.person}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Character'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            SizedBox(height: 24),
            Text(
              person.name,
              style: TextStyle(
                fontSize: 28,
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 12),
            Container(
              child: PersonCacheImage(
                imageUrl: person.image,
                height: 260,
                width: 260,
              ),
            ),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 12,
                  width: 12,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                    color:
                        person.status == 'Alive' ? Color(0xFF00ff00) : Colors.red,
                  ),
                ),
                SizedBox(width: 8),
                Text(
                  person.status,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                  maxLines: 1,
                ),
              ],
            ),
            SizedBox(height: 16),
            if(person.type.isNotEmpty )
            ...buidText('Type:', person.type),
            ...buidText('Gender:', person.gender),
            SizedBox(height: 6),
            ...buidText('Nomber of episodes:', person.episode.length.toString()),
            SizedBox(height: 6),
            ...buidText('Species:', person.species),
            SizedBox(height: 6),
            ...buidText('Last known location:', person.location.name),
            SizedBox(height: 6),
            ...buidText('Origin:', person.origin.name),
            SizedBox(height: 6),
            ...buidText('Was created:', person.created.toString()),
          ],
        ),
      ),
    );
  }

  List<Widget> buidText(String text, String value) {
    return [
      Text(
        text,
        style: TextStyle(
          color: AppColors.greyColor,
        ),
        maxLines: 1,
      ),
      SizedBox(height: 4),
      Text(
        value,
        style: TextStyle(
          color: Colors.white,
          fontSize: 16,
        ),
        maxLines: 1,
      ),
    ];
  }
}
