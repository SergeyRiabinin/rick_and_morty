import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/core/error/failure.dart';
import 'package:rick_and_morty/feature/domain/usecases/search_person.dart';
import 'package:rick_and_morty/feature/presentaition/bloc/search_bloc/search_event.dart';
import 'package:rick_and_morty/feature/presentaition/bloc/search_bloc/search_state.dart';

class PersonSearchBloc extends Bloc<PersonSearchEvent, PersonSearchState> {
  final SearchPerson searchPerson;
  PersonSearchBloc({required this.searchPerson}) : super(PersonEmptyState());

  @override
  Stream<PersonSearchState> mapEventToState(PersonSearchEvent event) async* {
    if (event is SearchPersonsEvent) {
      yield* _mapFetchPersonsToState(event.personQuery);
    }
  }

  Stream<PersonSearchState> _mapFetchPersonsToState(String personQuery) async* {
    yield PersonSearchLoadingState();

    final failureOrPerson =
        await searchPerson(SearchPersonParams(query: personQuery));

    yield failureOrPerson.fold(
        (failure) =>
            PersonSearchErrorState(message: _mapFailureToMessage(failure)),
        (person) => PersonSearchLoadedState(persons: person));
  }

  String _mapFailureToMessage(Failure failure) {

    switch (failure.runtimeType) {
      case ServerFailure:
        return 'Server Failure';
        case CacheFailure:
        return 'Cache Failure';
      default:
      return 'Unexpected Error';
    }
  }
}
