

import 'package:equatable/equatable.dart';

abstract class PersonSearchEvent extends Equatable{
  const PersonSearchEvent();

  List<Object>get props => [];
}

class SearchPersonsEvent extends PersonSearchEvent{
  final String personQuery;

  SearchPersonsEvent(this.personQuery);
  List<Object>get props => [personQuery];
}