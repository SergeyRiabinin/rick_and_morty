import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty/feature/presentaition/bloc/search_bloc/search_bloc.dart';
import 'package:rick_and_morty/feature/presentaition/bloc/search_bloc/search_event.dart';
import 'package:rick_and_morty/feature/presentaition/bloc/search_bloc/search_state.dart';
import 'package:rick_and_morty/feature/presentaition/widgets/search_result.dart';

class CustomSearcDelegate extends SearchDelegate {
  CustomSearcDelegate() : super(searchFieldLabel: 'Search for characters...');

  final _suggestions = [
    'Rick',
    'Morty',
    'Summer',
    'Beth',
    'Jerry',
  ];

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
            showSuggestions(context);
          },
          icon: Icon(Icons.clear))
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () {
        close(context, null);
      },
      tooltip: 'Back',
      icon: Icon(Icons.arrow_back_outlined),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    print('inside customSearchDelegate and search query is $query');
    BlocProvider.of<PersonSearchBloc>(context, listen: false)..add(SearchPersonsEvent(query));
    return BlocBuilder<PersonSearchBloc, PersonSearchState>(
        builder: (context, state) {
      if (state is PersonSearchLoadingState) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else if (state is PersonSearchLoadedState) {
        final persons = state.persons;
        if(persons.isEmpty){
          return _showEroorText('No Characters with that name found');
        }
        return Container(
          child: ListView.builder(
              itemCount: persons.isNotEmpty ? persons.length : 0,
              itemBuilder: (context, index) {
                PersonEntity result = persons[index];
                return SearchResult(personResult: result);
              }),
        );
      } else if (state is PersonSearchErrorState) {
        return _showEroorText(state.message);
      } else {
        return Icon(Icons.now_wallpaper);
      }
    });
  }

  Widget _showEroorText(String errorMessage) {
    return Container(
      color: Colors.black,
      child: Center(
        child: Text(
          errorMessage,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.length > 0) {
      return Container();
    }

    return ListView.separated(
      padding: EdgeInsets.all(10),
      itemBuilder: (context, index) {
        return Text(
          _suggestions[index],
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),
        );
      },
      separatorBuilder: (context, index) {
        return Divider();
      },
      itemCount: _suggestions.length,
    );
  }
}
