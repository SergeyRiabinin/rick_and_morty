

import 'dart:convert';

import 'package:rick_and_morty/core/error/exeption.dart';
import 'package:rick_and_morty/feature/data/models/person_model.dart';
import 'package:rick_and_morty/feature/domain/usecases/search_person.dart';
import 'package:http/http.dart' as http;

abstract class PersonRemoteDataSourse{

/// calls the https://rickandmortyapi.com/api/character/?page=1 endpoint
///
///Throws a [ServerException] for all error codes.

  Future<List<PersonModel>>gelAllPersons(int page);

/// calls the https://rickandmortyapi.com/api/character/?name=rick endpoint
///
///Throws a [ServerException] for all error codes.

  Future<List<PersonModel>>searchPerson(String query);
}

class PersonRemoteDataSourseImpl implements PersonRemoteDataSourse{
  final http.Client client;

  PersonRemoteDataSourseImpl({required this.client});
  
  @override
  Future<List<PersonModel>> gelAllPersons(int page) =>
  _getPersonFromUrl('https://rickandmortyapi.com/api/character/?page=$page');
  
  @override
  Future<List<PersonModel>> searchPerson(String query) =>
  _getPersonFromUrl('https://rickandmortyapi.com/api/character/?name=$query');
    
  

  Future<List<PersonModel>>_getPersonFromUrl(String url) async {
print(url);
final response = await client.get(Uri.parse(url),
    headers:{'ContentType': 'application/json' });

    if(response.statusCode == 200){
      final person = json.decode(response.body);
      return(person['results'] as List).map((person) => PersonModel.fromJson(person)).toList();
    }
    else{
      throw ServerException();
    }
  }
  
}